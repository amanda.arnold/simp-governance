# SIMP Project Bylaws

## Definitions

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in [RFC 2119](https://tools.ietf.org/html/rfc2119).

## Article I:   Membership

Individual memberships SHALL be decided on by a majority vote of the Council.

## Article II:  Member Meetings

Member Meetings MAY be held from time to time as deemed necessary by the Council.

## Article III: SIMP Council

### 3.1 Number of Councilmembers

The number of Councilmembers SHALL be five (5).

### 3.2 Chairperson

A Council Chairperson ("Chair," "Chairperson," "Chairman," or "Chairwoman") SHALL be elected by a majority vote of the Council.

### 3.3 Council Meetings

#### 3.3.1 Regular Meetings

Regular Meetings SHALL take place at least once every calendar quarter.
Regular Meetings MAY be held as frequently as deemed necessary by the Council.

#### 3.3.2 Special Meetings

A Special meeting is held outside the schedule of a Regular Meeting.
Special meetings MAY be called at any time by the Chair or by a majority of the Council.

#### 3.3.3 Notice of Meetings

Regular Meetings MUST be announced no less than one (1) week in advance.
Such notices SHOULD be posted to the *simp-dev* mailing list, and MAY be given personally, or by telephone, email, or electronic chat.
Special Meetings SHOULD be announced as soon as the need arises.
Special Meetings MAY be held without notice.

#### 3.3.4 Quorum

Fifty-percent of the Councilmembers then in office SHALL be necessary to constitute a quorum for the transaction of business except adjourn.

#### 3.3.7 Voting

A majority vote of active Councilmembers then in office SHALL be necessary to approve a resolution.
Any resolution receiving at least one (1) vote in the negative SHALL be tabled for further discussion.
Votes MAY be made in absentia.
Votes SHOULD be tallied via GitLab.

#### 3.3.8 Meeting Records

Meeting minutes and voting records for each meeting SHALL be published within one (1) week of the meeting.
Meeting minutes and voting records for each meeting SHOULD be published within one (1) week of the meeting to the *simp-dev* mailing list.

### 3.4 Vacancy

#### 3.4.1 Removals

A Councilmember MUST be removed by a vote of the entire Council, minus one (1).

### 3.4.2 Filling of Vacancies

Council offiices MUST be filled within one (1) month of vacancy.
The SIMP Council MUST call a special vote to fill such vacancies.

## Article IV.  Committees & Officers

### 4.1 Appointment of Committees & Officers

The Council MAY appoint such Committees or Officers as the Council deems necessary or appropriate.
Each such Committee or Officer SHALL have the authority and perform the duties as prescribed by the Council in the resolution establishing such Committee or Office.
Any Committee having authority of the Council SHALL consist of two (2) or more Committee Members.
Council Members and Officers SHALL be appointed by a majority vote of the Council and serve at the pleasure of the Council.

### 4.2 Powers and Authority of Committees & Officers

The Council may delegate to any Committee or Officer any of the powers and authority of the Council; provided, however, that no Committee or Officer may: (a) elect, appoint, or remove Councilmembers, Committee Members, or Officers; or (b) adopt, amend or repeal the Bylaws, or any resolution of the Council.

### 4.3 Committee Bylaws

Committees SHALL inherit sections 3.2 through 3.4.2 of these Bylaws unless otherwise stated in their respective charter resolutions.
