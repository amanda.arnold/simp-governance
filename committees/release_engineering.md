## The SIMP Release Engineering Committee

## Charge

The SIMP Release Engineering Committee ("REC") is responsible for advising the SIMP Council on issues related to release engineering, including process, tools, testing, and policy.

## Powers & Responsibilities

The REC is delegated the authority of the SIMP Council to make decisions and policy in the following domains:

  * Versioning and version control
  * Issue tracking
  * Metadata management
  * Build automation
  * Software testing

Decisions which may have an impact beyond these areas, or which require long-term planning (one year or longer), must be referred to the SIMP Council.
The interpretation of such is left to the REC, however, the SIMP Council reserves the right to review any and all decisions of the REC.

## Meetings

The REC shall meet no less than once per calendar month, or when necessary at the call of the committee chair.
Meeting minutes shall be posted to the *simp-dev* mailing list within one week of each meeting to record the decisions of the REC.

## Members

 * Chris Tessmer (Chair)
 * Dylan Cochran
 * Judy Johnson
 * Lucas Yamanishi
